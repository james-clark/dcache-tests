# dcache-tests

Scripts, submit files for testing dcache solutions for IGWN workflows

## Preliminaries: access

**SUMMARY**: access to the Nikhef dcache requires a certificate with a Virgo
VOMS extenstion; LIGO proxy certificates produced with `ligo-proxy-init` do not
work.

From Mary, the available endpoints are:

```console
davs://virgo.dcache.nikhef.nl:2880/pnfs/nikhef.nl/data/virgo/pegasusdisk
gsiftp://virgo.dcache.nikhef.nl:2811/pnfs/nikhef.nl/data/virgo/pegasusdisk
root://virgo.dcache.nikhef.nl:1094/pnfs/nikhef.nl/data/virgo/pegasusdisk
```

### Result with LIGO Proxy

**Summary**: access to Nikhef dcache fails with a ligo proxy.  Full verbosity
logs available
[here](https://git.ligo.org/james-clark/dcache-tests/-/tree/master/ligo-proxy-access).

Initialise an RFC 3820 compliant LIGO proxy:

```console
$ ligo-proxy-init -p james.clark
Your identity: james.clark@LIGO.ORG
Enter pass phrase for this identity:
Creating proxy .................................... Done
Your proxy is valid until: Feb 9 07:54:36 2021 GMT
$ grid-proxy-info 
subject  : /DC=org/DC=cilogon/C=US/O=LIGO/CN=James Clark
james.clark@ligo.org/CN=1976056659
issuer   : /DC=org/DC=cilogon/C=US/O=LIGO/CN=James Clark james.clark@ligo.org
identity : /DC=org/DC=cilogon/C=US/O=LIGO/CN=James Clark james.clark@ligo.org
type     : RFC 3820 compliant impersonation proxy
strength : 2048 bits
path     : /tmp/x509up_u40348
timeleft : 275:59:47  (11.5 days)
```

Access fails:

```console
$ gfal-stat -v https://virgo.dcache.nikhef.nl:2880/pnfs/nikhef.nl/data/virgo/pegasusdisk/
gfal-stat error: 13 (Permission denied) - Result HTTP 401 : Authentification
Error  after 1 attempts
```

with an identical result for DAV.

Try again with the other protocols:

```console
# GridFTP
$ gfal-stat -v gsiftp://virgo.dcache.nikhef.nl:2811/pnfs/nikhef.nl/data/virgo/pegasusdisk
gfal-stat error: 70 (Communication error on send) - globus_ftp_client: the
server responded with an error 530 Login denied
# ROOT
$ gfal-stat root://virgo.dcache.nikhef.nl:1094/pnfs/nikhef.nl/data/virgo/pegasusdisk
gfal-stat error: 13 (Permission denied) - Failed to stat file (Permission
denied)
```

### InCommon & Virgo VOMS

**Summary**: access to Nikhef dcache succeeds if, and only if, the proxy used has a
VOMS extension.  Full verbosity logs
available[here](https://git.ligo.org/james-clark/dcache-tests/-/tree/master/rucio-proxy-access).

I have an InCommon certificate which is frequenly used for rucio data
replication to CNAF and is registered with the Virgo VOMS:

```console
$ openssl x509 -in usercert.pem  -subject -enddate -noout subject= /DC=org/DC=incommon/C=US/ST=California/L=Pasadena/O=California Institute of Technology/OU=Laser Interferometer Gravitational-Wave Observatory/CN=rucio.ligo.caltech.edu
notAfter=May 28 23:59:59 2021 GMT
```

First, let's try it without the VOMS extension:

```console
$ voms-proxy-init -cert ./usercert.pem -key ./userkey.pem -out x509up
Your identity: /DC=org/DC=incommon/C=US/ST=California/L=Pasadena/O=California Institute of Technology/OU=Laser Interferometer Gravitational-Wave Observatory/CN=rucio.ligo.caltech.edu
Creating proxy ................................................. Done

Your proxy is valid until Fri Jan 29 00:16:16 2021

$ gfal-stat -v https://virgo.dcache.nikhef.nl:2880/pnfs/nikhef.nl/data/virgo/pegasusdisk                       
gfal-stat error: 13 (Permission denied) - Result HTTP 401 : Authentification
Error  after 1 attempts
```

...same result as above

Try again with the VOMS extension and it works:

```console
$ voms-proxy-init -voms "virgo:/virgo/virgo" -cert ./usercert.pem -key ./userkey.pem -out x509up
Your identity: /DC=org/DC=incommon/C=US/ST=California/L=Pasadena/O=California Institute of Technology/OU=Laser Interferometer Gravitational-Wave Observatory/CN=rucio.ligo.caltech.edu
Creating temporary proxy ...................... Done
Contacting  voms.cnaf.infn.it:15009 [/DC=org/DC=terena/DC=tcs/C=IT/L=Frascati/O=Istituto Nazionale di Fisica Nucleare/OU=CNAF/CN=voms.cnaf.infn.it] "virgo" Done
Creating proxy ......................................................... Done

Your proxy is valid until Fri Jan 29 00:18:34 2021

$ gfal-stat -v https://virgo.dcache.nikhef.nl:2880/pnfs/nikhef.nl/data/virgo/pegasusdisk 
  File: 'https://virgo.dcache.nikhef.nl:2880/pnfs/nikhef.nl/data/virgo/pegasusdisk'
  Size: 0   regular file
Access: (0755/-rwxr-xr-x)   Uid: 0  Gid: 0  
Access: 1969-12-31 16:00:00.000000
Modify: 1969-12-31 16:00:00.000000
Change: 1969-12-31 16:00:00.000000

```

including write access:
```
$ gfal-mkdir -v https://virgo.dcache.nikhef.nl:2880/pnfs/nikhef.nl/data/virgo/pegasusdisk/.james-test

$ gfal-copy input.txt https://virgo.dcache.nikhef.nl:2880/pnfs/nikhef.nl/data/virgo/pegasusdisk/.james-test/input.txt
Copying file:///home/james.clark/Projects/osg-check/dcache-tests/rucio-proxy-access/input.txt   [DONE]  after 1s 
```


